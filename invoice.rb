#!/usr/bin/env ruby

# try to make a sparkfun invoice using prawn

require 'rubygems'
require 'json'
require 'prawn'

class SparkPDF

  def spit (structure)
    Prawn::Document.generate('testinvoice.pdf', :page_layout => :portrait) do
      font "Courier"
      text "SparkFun Invoice", :at => [10, 20]
      image "logo.png", :width => 84, :height => 52

      structure.each do |thing|
        text thing
      end

      number_pages "<page> of <total>", [bounds.right - 50, bounds.top - 10] 
    end
  end

end

# test code
fakeinput = JSON ['bar', 'foo', 'baz']
structure = JSON fakeinput
document = SparkPDF.new
document.spit(structure)

